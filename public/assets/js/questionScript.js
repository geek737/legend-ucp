$(".rmv").click(function(e) {
    e.preventDefault();
    let idButton = '#'+e.target.id;
    let idImage = idButton.replace('Remove', '');
    let pathName = window.location.pathname;
    let idQuestion = pathName.match(/\d+/)[0];
    $.ajax({
        url:'/panel/questions/removeImage',
        type: 'POST',
        dataType: 'json',
        async: true,
        data: {
            'image' : $(idImage).attr('src'),
            'idQuestion' : idQuestion
        },
        success: function (data)
        {
            console.log(data);
        },
        error : function(xhr, textStatus, errorThrown) {
            console.log(xhr, textStatus, errorThrown);
        }
    });
    $("#imag").val("");
    $(idImage).attr("src", "");
    $(idImage).removeClass('it');
    $(idButton).removeClass('rmv');
    $(idButton).addClass('btn-rmv');

    return false;
});