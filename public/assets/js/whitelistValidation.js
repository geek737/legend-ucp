$("#rejected-button").click(function(e) {
    e.preventDefault();
    let idButton = $(this);
    let username = idButton.data('username');
    let firstName = idButton.data('firstname');
    let lastName = idButton.data('lastname');
    let birthDate = idButton.data('birthdate');
    let steamId = idButton.data('steamid');
    let userId = idButton.data('userid');
    let statusWhitelist = idButton.data('status');
    let response = $('#rejected-reason').val();
    if (response.trim() === '') {
        $('.error-validation').show();
    } else {
        $.ajax({
            url:'/panel/listusers/change_status_whitelist',
            type: 'POST',
            dataType: 'json',
            async: true,
            data: {
                'username' : username,
                'userId' : userId,
                'firstName' : firstName,
                'lastName' : lastName,
                'birthDate' : birthDate,
                'statusWhitelist' : statusWhitelist,
                'steamId' : steamId,
                'response' : response
            },
            success: function ()
            {
                // success
                location.reload();
            },
            error : function(xhr, textStatus, errorThrown) {
                console.log(xhr, textStatus, errorThrown);
            }
        });
        $('#modalRejected').modal('hide');
    }


    return false;
});

$("#rewrite-whitelist").click(function(e) {
    e.preventDefault();
    $.ajax({
        url:'/rewrite_whiteList',
        type: 'POST',
        dataType: 'json',
        async: true,
        success: function ()
        {
            // success
            location.reload();
        },
        error : function(xhr, textStatus, errorThrown) {
            console.log(xhr, textStatus, errorThrown);
        }
    });

    return false;
});

$("a[data-target='#modalRejected']").click(function (e) {
    console.log('Helios')
    let modal = $('.modal');
    // if (!modal.hasClass('show')) {
        modal.addClass('show');
    // }
    // if (modal.css('display') === 'none') {
        modal.css('display', 'block');
    // }
})
$(".modal .close").click(function (e) {
    $('.modal-backdrop').remove();
    $('body').removeClass('modal-open');
    $('.modal').removeClass('show');
    $('.modal').css('display', 'none');
})



