$("#applyDateChange").click(function(e) {
    let dateBegin = $('#date-begin').val();
    let dateEnd = $('#date-end').val();
    let job = $(this).attr('data-job');
    $.ajax({
        url:'/company/'.concat(job, '/search'),
        type: 'POST',
        dataType: 'json',
        data: {
            'dateBegin' : dateBegin,
            'dateEnd' : dateEnd
        },
        success: function (data)
        {
            if ($('#date-begin').val() > $('#date-end').val()) {
                $('.date-error').fadeIn(1000).css('display', 'flex');
            }else if ($('#logo-load').css('display') === 'none') {
                $('.date-error').hide();
                $('.no-data').hide();
                $('.resultat').empty();
                $('#logo-load').fadeIn(1000).css('display', 'flex');
                $('#logo-load img').addClass('spiner-legend');
                setTimeout(function() {
                    $("#logo-load").fadeOut(500);
                }, 500);
                setTimeout(function() {
                    $('.resultat').fadeIn(2500).append(data['view']);
                }, 1500);
            }
        },
        error : function(xhr, textStatus, errorThrown) {
            console.log(xhr, textStatus, errorThrown);
        }
    });
});

