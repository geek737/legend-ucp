<?php

namespace App\Form;

use App\Model\WhiteListResponse;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WhiteListResponseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('firstName', TextType::class , [
            'label' => false, ]);
        $builder->add('lastName', TextType::class , [
            'label' => false, ]);
        $builder->add('birthDate', TextType::class , [
            'label' => false, ]);
        $builder->add('response', TextareaType::class , [
            'label' => false, ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => WhiteListResponse::class,
        ]);
    }

}