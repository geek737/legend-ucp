<?php

namespace App\Form;

use App\Model\WhiteList;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class QuestionnaireType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('whiteListResponse',CollectionType::class,[
            'entry_type'=>WhiteListResponseType::class,
            'entry_options' => ['label' => false],
            'allow_delete' => true,
            'attr' => [
                'class' => 'form'
            ]
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => WhiteList::class,
        ]);
    }
}
