<?php

namespace App\Form;

use App\Model\WhiteList;
use App\Security\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class ProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('email', TextType::class, [
            'label' => false,
            'attr'=> [ 'readonly' => true]
        ])
            ->add('userNameGame', TextType::class, [
            'label' => false,
            'attr'=> [ 'readonly' => true]
        ])
            ->add('steamId', TextType::class, [
            'label' => false,
            'attr'=> [ 'readonly' => true]
        ])
            ->add('discordId', TextType::class, [
            'label' => false,
            'attr'=> [ 'readonly' => true]
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}