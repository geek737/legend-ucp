<?php

namespace App\Form;

use App\Model\WhiteListQuestion;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class WhiteListQuestionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', TextType::class , [
            'label' => false, ])
            ->add('question', TextareaType::class, [
                'label' => false,
                'attr' => [
                    'placeholder' => "Entrer votre question",
                ],
            ])
            ->add('language', ChoiceType::class, [
                'label' => false,
                'choices'  => [
                    'Francais' => 'FR',
                    'Anglais' => 'EN',
                ],
                'expanded' => true
            ])
            ->add('image', FileType::class, [
                'label' => false,
                'attr' => [
                    'accept' => "image/png, image/jpeg, image/jpg",
                ],
                // unmapped means that this field is not associated to any entity property
                'mapped' => false,

                // make it optional so you don't have to re-upload the PDF file
                // every time you edit the Product details
                'required' => false,

                // unmapped fields can't define their validation using annotations
                // in the associated entity, so you can use the PHP constraint classes
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' =>[
                            'image/jpeg',
                            'image/png',
                            'image/jpg',
                        ],
                        'mimeTypesMessage' => 'Veuillez upload une image valide',
                    ])
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => WhiteListQuestion::class,
        ]);
    }

}