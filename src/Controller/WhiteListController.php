<?php

namespace App\Controller;

use App\Form\QuestionnaireType;
use App\Model\Language;
use App\Security\User;
use App\Service\ApiRequest;
use App\Service\WhiteListService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class WhiteListController extends AbstractController
{
    /**
     * @Route("/whitelist", name="white_list")
     */
    public function index(
        Request $request,
        ApiRequest $apiRequest,
        WhiteListService $whiteListService
    ): Response {
        if (!$this->getUser()) {
            return $this->redirectToRoute('app_login');
        } else {
            if (empty($this->getUser()->getSteamId()) || empty($this->getUser()->getDiscordId())) {
                return $this->redirectToRoute('auth_steam_discord');
            } else {
                if ($this->getUser()->getStatsWhitelist() !== 'none'){

                    return $this->redirectToRoute('index');
                }
                $languageCode = 'FR';
                if (!empty($_GET['language'])) {
                    // May be control if he send language not exist
                    $languageCode = \strtoupper($_GET['language']);
                    if (!in_array($languageCode, ['FR', 'EN'])) {
                        $languageCode = 'FR';
                    }
                }
                $verifyLanguage = $apiRequest->findBy(
                    'languages',
                    'code',
                    $languageCode
                )->getBody()->getContents();
                $language = new Language();
                $LanguageAPI = \json_decode($verifyLanguage)[0];
                $mapper = new \JsonMapper();
                $mapper->map(
                    $LanguageAPI,
                    $language
                );
                // Find all questions
//                $responseQuestions = $apiRequest->findBy(
//                    'white_list_surveys',
//                    'language.id',
//                    $language->getId()
//                );
                $responseQuestions = $apiRequest->get(
                    'white_list_surveys',
                    18
                );

                // Add questions to Questionnaire
                $questionnaire = $whiteListService->addQuestionnaire($responseQuestions, $this->getUser());

                // Create now form Questionnaire
                $form = $this->createForm(QuestionnaireType::class, $questionnaire);

                // Send information to form
                $form->handleRequest($request);

                if ($form->isSubmitted()) {
                    if ($form->isValid()) {
                        $whiteListResponse = $questionnaire->getWhiteListResponse()->getValues()[0];
                        $response = $whiteListResponse->getResponse();
                        $firstName = $whiteListResponse->getFirstName();
                        $lastName = $whiteListResponse->getLastName();
                        $birthDate = $whiteListResponse->getBirthDate();
                        if (
                            strip_tags($response) !== $response ||
                            strip_tags($firstName) !== $firstName ||
                            strip_tags($lastName) !== $lastName ||
                            strip_tags($birthDate) !== $birthDate
                        ) {
//                            $data = ["statsWhitelist" => "rejected"];
//                            $apiRequest->patch('users', $data, (int) $this->getUser()->getId());
//                            $this->getUser()->setStatsWhiteList('rejected');
                            $this->addFlash('Veuillez réessayer (éviter les caractères spéciaux)');

                            return $this->redirectToRoute('index');
                        }
                        $whiteListResponse->setUser("api/users/" . $whiteListResponse->getUser()->getId());
                        $whiteListResponse->setQuestion(
                            'api/white_list_surveys/' . $whiteListResponse->getQuestion()->getId()
                        );
                        $responseToAdd = '['.$firstName.';' .$lastName.';'.$birthDate.']';
                        $whiteListResponse->setResponse($responseToAdd.$whiteListResponse->getResponse());
                        $response = $apiRequest->post(
                            'white_list_responses',
                            $whiteListResponse->getWhiteListResponseArray(),
                            true
                        );
                        if ($response->getStatusCode() !== 201) {
                            $this->addFlash('error', 'Veuillez réessayer');

                            return $this->redirectToRoute('white_list');
                        }
//                        }
                        $data = ["statsWhitelist" => "pending"];
                        $apiRequest->patch('users', $data, (int) $this->getUser()->getId());
                        $this->getUser()->setStatsWhitelist('pending');

                        return $this->redirectToRoute('index');
                    }
                }
            }

            return $this->render('white_list/index.html.twig', [
                'controller_name' => 'WhiteListController',
                'form' => $form->createView()
            ]);
        }
    }
}
