<?php

namespace App\Controller;

use App\Entity\Users;
use App\Form\ProfileType;
use App\Model\Feedback;
use App\Model\WhiteListResponse;
use App\Repository\AddonAccountRepository;
use App\Repository\UsersRepository;
use App\Security\User;
use App\Service\ApiRequest;
use App\Service\CompanyService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class PanelController
 * @package App\Controller
 *
 * @Route ("/panel")
 */
class PanelController extends AbstractController
{
    /**
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_WHITLISTED') or is_granted('ROLE_WHITLISTER')")
     * @Route("/", name="panel")
     */
    public function indexAction(ApiRequest $apiRequest): Response
    {
        $whitelistResponsesCount = \count(
            \json_decode($apiRequest->findAll('white_list_responses')->getBody()->getContents())
        );

        return $this->render('panel/index.html.twig', [
            'whitelistResponseCount' => $whitelistResponsesCount
        ]);
    }

    /**
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_WHITLISTER')")
     * @Route("/listusers", name="listusers_list")
     */
    public function displayUsersWhitelistedAction(ApiRequest $apiRequest): Response
    {
        $allUsers = [];
        $users = \json_decode($apiRequest->findAll('users')->getBody()->getContents());
        foreach ($users as $user) {
            $newUser = new User();
            $mapper = new \JsonMapper();
            $mapper->map(
                $user,
                $newUser
            );
            $allUsers[] = $newUser;
        }

        return $this->render('white_list/user_list.html.twig', [
            'list_users' => $allUsers
        ]);
    }

    /**
     * @Route("/listusers/change_status_whitelist", name="change_status_whitelist")
     */
    public function changeStatusWhitelistAction(Request $request, ApiRequest $apiRequest)
    {
        $status = $request->get('statusWhitelist');
        $userId = $request->get('userId');
        $responseReason = $request->get('response');
        $dataUser = ["statsWhitelist" => $status];
        $dataFeedback = [
            "user" => "api/users/".$userId,
            "comment" => 'Rejected : '.\trim($responseReason),
            "author" => "api/users/".$this->getUser()->getId(),
            "createdAt" => \date("Y-m-d H:i:s")
        ];
        $userResponseFromApi = $apiRequest->patch('users', $dataUser, (int) $userId);
        $feedbackResponseFromApi = $apiRequest->post('feed_backs', $dataFeedback);

        if ($userResponseFromApi->getStatusCode() !== 200 || $feedbackResponseFromApi->getStatusCode() !== 201) {
            $response = "Success";
        } else {
            $response = "Error";
        }

        return new JsonResponse($response);
    }

    /**
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_WHITLISTER')")
     * @Route("/listusers/{username}", name="listusers_user_validation")
     */
    public function whiteListValidationAction(
        $username,
        ApiRequest $apiRequest,
        Request $request,
        EntityManagerInterface $manager,
        UsersRepository $repository
    ) {
        // Get user
        $user = new User();
        $firstName = '';
        $lastName = '';
        $birthDate = '';
        $response =  $apiRequest->findBy('users', 'userNameGame', $username);
        $userAPI = \json_decode($response->getBody()->getContents())[0];
        $mapper = new \JsonMapper();
        $mapper->map(
            $userAPI,
            $user
        );
        $responses = [];
        if (!$this->getUser()) {
            return $this->redirectToRoute('app_login');
        }
        if (!empty($_GET['status'])) {
            // May be control if he send language not exist
            $status = \strtolower($_GET['status']);
            $data = ["statsWhitelist" => $status];
            $responseFromApi = $apiRequest->patch('users', $data, (int) $user->getId());
            var_dump($responseFromApi->getBody()->getContents());die;
            if ($responseFromApi->getStatusCode() !== 200 || empty($_GET['firstName'])) {
                $this->addFlash('error', 'Changement de statut a été échoué');
            } else {
                $isExisted = $repository->findOneBy(["identifier"=>$_GET['steam']]);
                if ($status === 'accepted' && empty($isExisted)) {
                    $users = new Users();
                    $users->setFirstName($_GET['firstName']);
                    $users->setLastName($_GET['lastName']);
                    $users->setBirthDate($_GET['birthDate']);
                    $users->setIdentifier($_GET['steam']);
                    $users->setJob('unemployed');
                    $users->setJobGrade(0);
                    $manager->persist($users);
                    $manager->flush();
                }
                $this->addFlash('success', 'Changement de statut a été bien effectué');
            }

            return $this->redirectToRoute('listusers_user_validation', ["username" => $username]);
        }

        // Get response whitelist
        $responsesApi = \json_decode(
            $apiRequest->findBy(
                'white_list_responses', 'user', '/api/users/'.$user->getId()
            )->getBody()->getContents()
        );

        // Get question whitelist and set questions in responses
        foreach ($responsesApi as $responseApi) {
                $response = new WhiteListResponse();
                $mapper = new \JsonMapper();
                $mapper->map(
                    $responseApi,
                    $response
                );
                $user->addWhiteListResponse($response);
                $responses[] = $response;
            \preg_match('/\[(.+);(.+);(\d+\/\d+\/\d+)\]((.|\n|\t)+)/', $response->getResponse(), $matches);
                if (!empty($matches)) {
                    $response->setResponse($matches[4]);
                    $firstName = $matches[1];
                    $lastName = $matches[2];
                    $birthDate = $matches[3];
                }
            }
        $responseFeedbackApi = \json_decode(
            $apiRequest->findBy(
                'feed_backs', 'user', '/api/users/'.$user->getId()
            )->getBody()->getContents()
        );
        $responseFeedback = [];
        if (!empty($responseFeedbackApi)) {
            $responseFeedback = new Feedback();
            $mapper = new \JsonMapper();
            $mapper->map(
                end($responseFeedbackApi),
                $responseFeedback
            );
        }

        return $this->render('white_list/whitelist_validation_of_users.html.twig', [
            'responses' => $responses,
            'user' => $user,
            'firstName' => $firstName,
            'lastName' => $lastName,
            'birthDate' => $birthDate,
            'feedbackResponse' => $responseFeedback
        ]);
    }

    /**
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_WHITLISTED') or is_granted('ROLE_WHITLISTER')")
     * @Route("/me", name="user_profile")
     */
    public function getProfileAction(Request $request, UsersRepository$usersRepository): Response
    {
        $user = $this->getUser();
        $result = $usersRepository->findOneBy(['identifier' => $user->getSteamId()]);
        /** TO DO lié avec la table job */
        $job = null;
        $rib = $money = null;
        if (count($result->getAddonAccounts()) === 1) {
            $rib = $result->getAddonAccounts()[0]->getRib();
            $money = $result->getAddonAccounts()[0]->getMoney();
        } else {
            foreach ($result->getAddonAccounts() as $account) {
                if ($account->getFaction() === null && $account->getCurrent() === true) {
                    $rib = $result->getAddonAccounts()[0]->getRib();
                    $money = $result->getAddonAccounts()[0]->getMoney();
                }
            }
        }

        $whiteListed = $user->getStatsWhitelist();
        if ($whiteListed !== 'accepted') {
            return $this->redirectToRoute('index');
        }
        $form = $this->createForm(ProfileType::class, $user);

        return $this->render('panel/profile/index.html.twig', [
            'form' => $form->createView(),
            'rib' => $rib,
            'money' => $money,
        ]);
    }

    /**
     * @Route("/test", name="test")
     */
    public function test(UsersRepository $repository, CompanyService $companyService, AddonAccountRepository $accountRepository)
    {
//        dd($repository->findAll());
        $dateBegin = strtotime('14-08-2022 00:00:00');
        $dateEnd =  strtotime('22-08-2022 23:59:59');
        $result = $repository->getPoliceUsers($dateBegin, $dateEnd, 'lspd');
        $array = [];
        foreach ($result as $item) {
            $array[] = $item->getIdentifier();
        }
//        $members_LSPD = $companyService->filterDataByDate($result, 'show_user_services', $dateBegin, $dateEnd);
//
        echo json_encode($array);die;

    }
}
