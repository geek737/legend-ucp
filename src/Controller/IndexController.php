<?php

namespace App\Controller;

use App\Model\Feedback;
use App\Service\ApiRequest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class IndexController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function indexAction(ApiRequest $apiRequest): Response
    {
        $message = '';
        $discord = 'https://discord.gg/QJFKvc3j4Z';
        $icon = null;
        $isRewriteWL  = false;
        if (!$this->getUser()) {
            return $this->redirectToRoute('app_login');
        }
        $discordId = $this->getUser()->getDiscordId();
        $steamId = $this->getUser()->getStatsWhitelist();
        if (!$discordId || !$steamId) {
            return $this->redirectToRoute('auth_steam_discord');
        }
        $whiteListed = $this->getUser()->getStatsWhitelist();
        if ($whiteListed === 'voice interview') {
            $message = 'Félicitations, vous êtes invité à passer un entretien vocal via';
        } elseif ($whiteListed === 'pending'){
            $message = 'Votre demande est en cours de traitement';
            $icon = 'bouncer';
            $discord = null;
        } elseif ($whiteListed === 'rejected') {
            $responseFeedbackApi =
                $apiRequest->findBy(
                    'feed_backs', 'user', '/api/users/'.$this->getUser()->getId()
                );
            $bodyContentFeedbackApi = \json_decode($responseFeedbackApi->getBody()->getContents());
            if (!empty($bodyContentFeedbackApi) && $responseFeedbackApi->getStatusCode() === 200) {
                $responseFeedback = new Feedback();
                $mapper = new \JsonMapper();
                $mapper->map(
                    end($bodyContentFeedbackApi),
                    $responseFeedback
                );
                \preg_match('/:(.+)/', $responseFeedback->getComment(), $matches);
                $isRewriteWL = true;
                $discord = null;
            }

            $messageRejected = isset($matches) ?
                \trim($matches[1]) :
                'Il n\'y a pas de message de refus veuillez contactez le support';
            $message = "Votre demande a été refusée  pour la raison suivante : \n" . $messageRejected;
        } elseif ($whiteListed === 'accepted') {
            $this->addFlash('success', 'Bienvenue sur Legends Roleplay');
            return $this->redirectToRoute('panel');
        }else {
            return $this->redirectToRoute('white_list');
        }

        return $this->render('white_list/statuts_whitelist.html.twig', [
            'message' => $message,
            'icon' => $icon,
            'discord' => $discord,
            'isRewriteWL' => $isRewriteWL
        ]);
    }

    /**
     * @Route("/rewrite_whiteList", name="rewrite_WhiteList")
     */
    public function rewriteWhiteList(Request $request, ApiRequest $apiRequest)
    {
        $data = ["statsWhitelist" => 'none'];
        $responseFromApi = $apiRequest->patch('users', $data, (int) $this->getUser()->getId());
        if ($responseFromApi->getStatusCode() === 200 ) {
            $this->getUser()->setStatsWhitelist('none');
        }
        return new JsonResponse('Repasser la whitelist');
    }
}
