<?php

namespace App\Controller;

use App\Form\VehiclesType;
use App\Repository\UsersRepository;
use App\Repository\VehiclesRepository;
use App\Service\ApiRequest;
use App\Service\CompanyService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Twig\Environment;

/**
 * Class CompanyController
 * @package App\Controller
 *
 *
 * @Route ("/company")
 */
class CompanyController extends AbstractController
{
    /**
     * @var ApiRequest
     */
    private $apiRequest;
    private $urlImagesVehicles;
    /**
     * @var Environment
     */
    private $environment;

    public function __construct(Environment $environment,ApiRequest $apiRequest, $urlImagesVehicles)
    {
        $this->apiRequest = $apiRequest;
        $this->urlImagesVehicles = $urlImagesVehicles;
        $this->environment = $environment;
    }

    /**
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_ECONOMY')")
     *
     * @Route("/concession", name="concession")
     *
     * @param VehiclesRepository $repository
     *
     *
     *
     * @return Response
     */
    public function concessionAction(VehiclesRepository $repository): Response
    {
        $vehicles = $repository->findAllOrderBy('category');
        return $this->render('company/vehicles_list.html.twig', [
            'list_vehicles' => $vehicles,
            'urlImages' => $this->urlImagesVehicles
        ]);
    }

    /**
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_ECONOMY')")
     *
     * @Route("/concession/{vehicle}", name="vehicle_page")
     *
     * @param $vehicle
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @param VehiclesRepository $repository
     *
     * @return Response
     */
    public function vehiclePageAction(
        $vehicle,
        Request $request,
        EntityManagerInterface $manager,
        VehiclesRepository $repository
    ): Response {
        $vehicleFound = $repository->findOneBy(["model"=>$vehicle]);
        $form = $this->createForm(VehiclesType::class, $vehicleFound);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $manager->persist($vehicleFound);
            $manager->flush();
            $this->addFlash('success', 'Changement de statut a été bien effectué');
        }

        return $this->render('company/vehicle_page.html.twig', [
            'form' => $form->createView(),
            'model' => $vehicleFound->getModel(),
            'urlImages' => $this->urlImagesVehicles
        ]);
    }

    /**
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_LSPD_BOSS')")
     *
     * @Route("/lspd", name="lspd")
     */
    public function policeAction()
    {
        return $this->render('company/lspd_list_members.html.twig');
    }

    /**
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_EMS_BOSS')")
     *
     * @Route("/ems", name="ems")
     */
    public function hospitalAction()
    {
        return $this->render('company/ems_list_members.html.twig');
    }

    /**
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_LSPD_BOSS')")
     *
     * @Route("/lspd/search", name="lspd_search", options={"expose"=true})
     *
     * @param Request $request
     * @param UsersRepository $repository
     * @param CompanyService $companyService
     *
     * @return JsonResponse
     */
    public function policeSearchAction(
        Request $request,
        UsersRepository $repository,
        CompanyService $companyService
    ): JsonResponse {
        $dateBegin = strtotime($request->request->get('dateBegin').' 00:00:00');
        $dateEnd =  strtotime($request->request->get('dateEnd').' 23:59:59');
        $result = $repository->getPoliceUsers($dateBegin, $dateEnd, 'lspd');
        $members_LSPD = $companyService->filterDataByDate($result, 'show_user_services', $dateBegin, $dateEnd);

        return new JsonResponse(
            ['view' => $this->render(
            'partials/_data_memebrs.html.twig',
            [
                'list_members' => $members_LSPD,
            ]
        )->getContent()]);
    }

    /**
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_EMS_BOSS')")
     *
     * @Route("/ems/search", name="ems_search", options={"expose"=true})
     *
     * @param Request $request
     * @param UsersRepository $repository
     * @param CompanyService $companyService
     *
     * @return JsonResponse
     */
    public function hospitalSearchAction(
        Request $request,
        UsersRepository $repository,
        CompanyService $companyService
    ): JsonResponse {
        $dateBegin = strtotime($request->request->get('dateBegin').' 00:00:00');
        $dateEnd =  strtotime($request->request->get('dateEnd').' 23:59:59');
        $result = $repository->getPoliceUsers($dateBegin, $dateEnd, 'ems');
        $members_EMS = $companyService->filterDataByDate($result, 'show_user_services', $dateBegin, $dateEnd);

        return new JsonResponse(
            ['view' => $this->render(
            'partials/_data_memebrs.html.twig',
            [
                'list_members' => $members_EMS,
            ]
        )->getContent()]);
    }
}
