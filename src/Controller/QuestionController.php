<?php

namespace App\Controller;

use App\Form\QuestionnaireType;
use App\Form\WhiteListQuestionType;
use App\Model\Language;
use App\Model\WhiteListQuestion;
use App\Service\ApiRequest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 * @Route("/panel/questions")
 */
class QuestionController extends AbstractController
{
    /**
     * @Route("/", name="questions")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_WHITLISTER')")
     */
    public function indexAction(ApiRequest $apiRequest): Response
    {
        // Find all questions
        $response = $apiRequest->findAll('white_list_surveys');
        $questionsApi = \json_decode($response->getBody()->getContents());
        $questions = [];
        foreach ($questionsApi as $question) {
            $newQuestion = new WhiteListQuestion();
            $mapper = new \JsonMapper();
            $mapper->map(
                $question,
                $newQuestion
            );
            $questions[] = $newQuestion;
        }

        return $this->render('question/index.html.twig', [
            'questions' => $questions,
            'navTabsActions' => [
                [
                    "name" => "Ajouter une question",
                    "route" => "question_add"
                ]
            ]
        ]);
    }

    /**
     * @Route("/add", name="question_add")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_WHITLISTER')")
     */
    public function addQuestionAction(Request $request, SluggerInterface $slugger, ApiRequest $apiRequest): Response {

        // Create now form Questionnaire
        $question = new WhiteListQuestion();
        $form = $this->createForm(WhiteListQuestionType::class, $question);

        // Send information to form
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                if (
                    strip_tags($question->getQuestion()) !== $question->getQuestion() ||
                    strip_tags($question->getTitle()) !== $question->getTitle()
                ) {
                    $this->addFlash('error', 'Veuillez réessayer');

                    return $this->redirectToRoute('question_add');
                }
                // Get url of Image
                $imageFile = $form->get('image')->getData();
                if ($imageFile !== null) {
                    $originalFilename = pathinfo($imageFile->getClientOriginalName(), PATHINFO_FILENAME);
                    $safeFilename = $slugger->slug($originalFilename);
                    $newFilename = $safeFilename.'-'.uniqid().'.'.$imageFile->guessExtension();
                    try {
                        $imageFile->move(
                            $this->getParameter('images_whitelist_directory'),
                            $newFilename
                        );
                        $question->addImage($newFilename);
                    } catch (FileException $e) {
                        // ... handle exception if something happens during file upload
                    }
                }

                // Update author
                $question->setAuthor("api/users/" . $this->getUser()->getId());

                // Get language
                $verifyLanguage = \json_decode($apiRequest->findBy(
                    'languages',
                    'code',
                    $question->getLanguage()
                )->getBody()->getContents())[0];

                // Update Language
                $question->setLanguage("api/languages/".$verifyLanguage->id);

                // Add question
                $response = $apiRequest->post(
                    'white_list_surveys',
                    $question->getWhiteListQuestionArray(),
                    true
                );
                if ($response->getStatusCode() !== 201) {
                    $this->addFlash('error', 'Veuillez réessayer');
                } else {
                    $this->addFlash('success', 'Votre question a été bien ajouter');
                }

                return $this->redirectToRoute('questions');
            }
        }

        return $this->render('question/add_question.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_WHITLISTER')")
     * @Route("/{id}", name="question", requirements={"id"="\d+"})
     */
    public function updateQuestionAction(Request $request,int $id, ApiRequest $apiRequest, SluggerInterface $slugger): Response
    {
        $question = new WhiteListQuestion();
        $questionAPI = \json_decode(
            $apiRequest->get('white_list_surveys', $id)->getBody()->getContents()
        );
        $mapper = new \JsonMapper();
        $mapper->map(
            $questionAPI,
            $question
        );
        $question->setLanguage($question->getLanguage()->code);
        $form = $this->createForm(WhiteListQuestionType::class, $question);
        // Send information to form
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                if (
                    strip_tags($question->getQuestion()) !== $question->getQuestion() ||
                    strip_tags($question->getTitle()) !== $question->getTitle()
                ) {
                    $this->addFlash('error', 'Veuillez réessayer');

                    return $this->redirectToRoute('question_add');
                }
                // Get url of Image
                $imageFile = $form->get('image')->getData();
                if ($imageFile !== null) {
                    $originalFilename = pathinfo($imageFile->getClientOriginalName(), PATHINFO_FILENAME);
                    $safeFilename = $slugger->slug($originalFilename);
                    $newFilename = $safeFilename.'-'.uniqid().'.'.$imageFile->guessExtension();
                    try {
                        $imageFile->move(
                            $this->getParameter('images_whitelist_directory'),
                            $newFilename
                        );
                        $question->addImage($newFilename);
                    } catch (FileException $e) {
                        // ... handle exception if something happens during file upload
                    }
                }

                // Get language
                $verifyLanguage = \json_decode($apiRequest->findBy(
                    'languages',
                    'code',
                    $question->getLanguage()
                )->getBody()->getContents())[0];

                // Update Language
                $question->setLanguage("api/languages/".$verifyLanguage->id);

                // Update information
                $data = [
                    'images' => $question->getImages(),
                    'title' => $question->getTitle(),
                    'question' => $question->getQuestion(),
                    'language' => $question->getLanguage(),
                ];
                $response = $apiRequest->patch('white_list_surveys', $data, (int) $question->getId());
                if ($response->getStatusCode() !== 200) {
                    $this->addFlash('error', 'Veuillez réessayer');
                } else {
                    $this->addFlash('success', 'Votre question a été bien modifier');
                }

                return $this->redirect($request->server->get('HTTP_REFERER'));
            }
        }

        return $this->render('question/show_question.index.html.twig', [
            'form' => $form->createView(),
            'images' => $question->getImages()
        ]);
    }

    /**
    * @Route("/removeImage", name="remove_image_question")
    */
    public function removeImageAction(Request $request, ApiRequest $apiRequest)
    {
        // Get id question and get this question
        $idQuestion = $request->get('idQuestion');
        $questionAPI = \json_decode(
            $apiRequest->get('white_list_surveys', $idQuestion)->getBody()->getContents()
        );
        $mapper = new \JsonMapper();
        $question = new WhiteListQuestion();
        $mapper->map(
            $questionAPI,
            $question
        );

        // get file name of this image and delete it
        preg_match("/brochures\/(.+)/", $request->get('image'), $matches);
        $fileNameImage = $matches[1];
        $removeStatus = $question->removeImage($fileNameImage);
        if ($removeStatus === true) {
            // Update information
            $data = [
                'images' => $question->getImages(),
            ];
            $response = $apiRequest->patch('white_list_surveys', $data, (int) $question->getId());
            if ($response->getStatusCode() !== 200) {
                return new JsonResponse('remove_image_fail');
            } else {
                return new JsonResponse('remove_image_success');
            }
        }

        return new JsonResponse('remove_image');
    }
}
