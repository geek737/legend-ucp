<?php


namespace App\Controller;


use App\Security\User;
use App\Service\ApiRequest;
use App\Service\Mailer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class MailerController
 * @package App\Controller
 * @Route("/mail", name="mail")
 */
class MailerController extends AbstractController
{

    /**
     * @Security("is_granted('ROLE_ADMIN')")
     * @Route("/interview_mailer", name="interview_mailer_send")
     */
    public function voiceInterviewMailerAction(Mailer $mailer, ApiRequest $apiRequest): Response
    {
        $allUsers = [];
        $users =  \json_decode(
            $apiRequest->findBy('users', 'statsWhitelist', 'voice interview')
            ->getBody()
            ->getContents()
        );
        foreach ($users as $user) {
            $newUser = new User();
            $mapper = new \JsonMapper();
            $mapper->map(
                $user,
                $newUser
            );
            $mailer->sendMail(
                $newUser->getEmail(),
                'Bienvenue à Legends Roleplay : Vous êtes invité à un entretien vocal',
                'mailer/interview_message_reminder.html.twig'
            );
            $allUsers[] = $newUser;
        }

        return $this->render(
            'mailer/message_send.html.twig',
                [
                    'list_users' => $allUsers,
                ]);
    }

}