<?php

namespace App\Controller;

use App\Form\ForgetPasswordType;
use App\Form\RegistrationType;
use App\Form\ResetPasswordType;
use App\Form\UserFormType;
use App\Security\User;
use App\Service\ApiRequest;
use App\Service\Mailer;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @var string
     */
    private $urlApi;
    /**
     * @var ApiRequest
     */
    private $apiRequest;

    public function __construct(string $urlApi, ApiRequest $apiRequest)
    {
        $this->urlApi = $urlApi;
        $this->apiRequest = $apiRequest;
    }

    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
         if ($this->getUser()) {
             return $this->redirectToRoute('index');
         }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();
        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    /**
     * @Route("/registration", name="app_register")
     * @param Request $request
     * @param UserPasswordHasherInterface $encoder
     * @param SessionInterface $session
     *
     * @return RedirectResponse|Response
     *
     * @throws GuzzleException
     */
    public function registration(
        Request $request,
        UserPasswordHasherInterface $encoder,
        SessionInterface $session
    )
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('index');
        }
        $user = new User();
        $form = $this->createForm(RegistrationType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $user->setCreatedAt(\date("Y-m-d H:i:s"));
                $user->setPriority("low");
                $user->setStatsWhitelist("none");
                $user->setSteamId(null);
                $user->setDiscordId(null);
                $user->setPassword($encoder->hashPassword($user, $user->getPassword()));
                $user->setPasswordChecked($user->getPassword());
                $user->setIsPasswordChanging(false);
                $user->addRole("ROLE_WHITLISTED");
                $session->set('User', $user);
                $response = $this->apiRequest->post('users', $user->getUserArray());
                $statusCode = $response->getStatusCode();
                if ($statusCode != 201) {
                    $message = \json_decode($response->getBody()->getContents())
                            ->violations[0]->message ?? 'une erreur s\'est produite  lors d\'inscription';
                    $this->addFlash('error', $message);

                    return $this->redirectToRoute('app_register');
                }
                $this->addFlash('success', 'Veuillez vous connecter');

                return $this->redirectToRoute('app_login');
            }
        }

        return $this->render('security/registration.html.twig', [
            'form'=>$form->createView()
        ]);
    }


    /**
     * @Route("/forget_password", name="forget_password")
     * @param Request $request
     * @param Mailer $mailer
     *
     * @return RedirectResponse|Response
     *
     * @throws GuzzleException
     * @throws \JsonMapper_Exception
     */
    public function forgetPassword(Request $request, Mailer $mailer) {

        if ($this->getUser()) {
            return $this->redirectToRoute('index');
        }
        $form = $this->createForm(ForgetPasswordType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $dataForm = $form->getData();
            $token = $this->apiRequest->generateToken();
            $response = $this->apiRequest->findBy('users', 'email', $dataForm['email'], $token);

            $userAPI = \json_decode($response->getBody()->getContents())[0];
            $user = new User();
            $mapper = new \JsonMapper();
            $mapper->map(
                $userAPI,
                $user
            );
            $dateDiff = intval(
                abs(strtotime(\date("Y-m-d H:i:s")) - strtotime($user->getUrlCreatedAt())) / (60 * 60)
            );
            if ($dateDiff >= 6) {
            $tokenPassword = bin2hex(\random_bytes(20));
            $data = [
                "isPasswordChanging" => true,
                "tokenChangingPassword" => $tokenPassword,
                "urlCreatedAt" => \date("Y-m-d H:i:s")
            ];
                $mailer->sendMailRestPasswordVerification($dataForm['email'], $tokenPassword);
                $this->apiRequest->patch('users', $data, (int) $user->getId(), $token);
                $this->addFlash(
                    'success',
                    'Un mail a été envoyé sur cette email, vérifiez vos courriers ou le courrier indésirable'
                );
            } else {
                $this->addFlash(
                    'error',
                    'Un mail a déjà été envoyé sur cette adresse, vérifiez vos courriers ou le courrier indésirable'
                );
            }

        }

        return $this->render('security/forget_password.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/reset-password/{token}", name="reset_password")
     */
    public function resetPassword(string $token, Request $request, UserPasswordHasherInterface $encoder)
    {
        $tokenSecurity = $this->apiRequest->generateToken();
        $response = $this->apiRequest->findBy('users', 'tokenChangingPassword', $token, $tokenSecurity);
        $userAPI = \json_decode($response->getBody()->getContents());
        if (empty($userAPI)) {
            $this->addFlash(
                'error',
                'une erreur est survenue veuillez réessayer'
            );

            return $this->redirectToRoute('app_login');
        }
        $user = new User();
        $mapper = new \JsonMapper();
        $mapper->map(
            $userAPI[0],
            $user
        );
        $dateDiff = intval(
            abs(strtotime(\date("Y-m-d H:i:s")) - strtotime($user->getUrlCreatedAt())) / (60 * 60)
        );
        if (!$user->getIsPasswordChanging() || empty($user->getTokenChangingPassword()) || $dateDiff >= 6) {
            $this->addFlash(
                'error',
                'Cette URL a expiré, veuillez réessayer'
            );

            return $this->redirectToRoute('app_login');
        }
        $form = $this->createForm(ResetPasswordType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $data = [
                "password" => $encoder->hashPassword($user, $user->getPassword()),
                "isPasswordChanging" => false,
                "tokenChangingPassword" => null,
            ];
            $response = $this->apiRequest->patch('users', $data, (int) $user->getId(), $tokenSecurity);
            if ($response->getStatusCode() === 200) {
                $this->addFlash(
                    'success',
                    'Votre mot de passe a été changé avec succès'
                );

                return $this->redirectToRoute('app_login');
            }
            $this->addFlash(
                'error',
                'une erreur est survenue veuillez réessayer'
            );
        }

        return $this->render('security/reset_password.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
