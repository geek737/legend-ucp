<?php

namespace App\Controller;

use App\Service\ApiRequest;
use App\SteamAuth\LightOpenID;
use http\Env\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use App\SteamAuth;


class AuthSteamDiscordController extends AbstractController
{
    /**
     * @var string
     */
    private $urlPanel;

    public function __construct(string $urlPanel)
    {

        $this->urlPanel = $urlPanel;
    }

    /**
     * @Route("/auth/connectAccount", name="auth_steam_discord")
     */
    public function indexAction(SessionInterface $session, ApiRequest $apiRequest): Response
    {
        if (!$session->has('User')) {
            $session->set('User', $this->getUser());
        }
        $sessionUser = $session->get('User');
        $steamHidden = (bool) $sessionUser->getSteamId();
        $discordHidden = (bool) $sessionUser->getDiscordId();
        if (!empty($_GET['action'])) {
            if ($_GET['action'] === 'steam') {
                //Version 4.0
                // Your Steam WebAPI-Key found at https://steamcommunity.com/dev/apikey
                $steamauth['apikey'] = "4567586D35E61215680BEAD4037694AE";

                // The main URL of your website displayed in the login page
                $steamauth['domainname'] = $this->urlPanel."steam/panel/connect_social_networks";

                // Page to redirect to after a successfull logout (from the directory the SteamAuth-folder is located in) - NO slash at the beginning!
                $steamauth['logoutpage'] = "../panel/demo.php";

                // Page to redirect to after a successfull login (from the directory the SteamAuth-folder is located in) - NO slash at the beginning!
                $steamauth['loginpage'] = "../panel/demo.php";

                // System stuff
                if (empty($steamauth['apikey'])) {
                    die("<div style='display: block; width: 100%; background-color: red; text-align: center;'>SteamAuth:<br>Please supply an API-Key!<br>Find this in SteamAuth/SteamConfig.php, Find the '<b>\$SteamAuth['apikey']</b>' Array. </div>");
                }
                if (empty($steamauth['domainname'])) {
                    $steamauth['domainname'] = $_SERVER['SERVER_NAME'];
                }
                if (empty($steamauth['logoutpage'])) {
                    $steamauth['logoutpage'] = $_SERVER['PHP_SELF'];
                }
                if (empty($steamauth['loginpage'])) {
                    $steamauth['loginpage'] = $_SERVER['PHP_SELF'];
                }
                try {
                    // Access to Steam
                    $openid = new LightOpenId($steamauth['domainname']);
                    if (!$openid->mode) {
                        $openid->identity = 'https://steamcommunity.com/openid';
                        return $this->redirect($openid->authUrl());
                        // Cancel access
                    } elseif ($openid->mode == 'cancel') {
                        $message = "Impossible d'effectuer la connexion à steam, veuillez ressayer à nouveau .";
                        $this->addFlash('error', $message);
                    } else {
                        if ($openid->validate()) {
                            $id = $openid->identity;
                            $ptn = "/^https?:\/\/steamcommunity\.com\/openid\/id\/(7[0-9]{15,25}+)$/";
                            preg_match($ptn, $id, $matches);
                            $steamId = $matches[1];
                            if (!empty($steamId)) {
                                $steamId = 'steam:' . dechex($steamId);
                                $steamExist = $apiRequest->findBy(
                                    'users',
                                    'steamId',
                                    $steamId
                                )->getBody()->getContents();
                                if ($steamExist !== "[]") {
                                    $this->addFlash('error', 'Ce Steam est deja lié a un autre compte');
                                    $this->redirectToRoute('auth_steam_discord');
                                } else {
                                    //Récupérer l'identifiant à partir de steam
                                    $data = [
                                        "steamId" => $steamId
                                    ];
                                    $response = $apiRequest->patch('users', $data, $sessionUser->getId());
                                    if ($response->getStatusCode() === 200) {
                                        $sessionUser->setSteamId($steamId);
                                        $this->getUser()->setSteamId($steamId);
                                        $steamHidden = true;
                                        $this->addFlash(
                                            'success',
                                            'Votre steam a été bien enregistrée'
                                        );
                                    } else {
                                        $this->addFlash('error', 'Veuillez esseyer');
                                    }
                                }
                            }
                        } else {
                            $message = "Impossible d'effectuer la connexion à steam, veuillez ressayer à nouveau .";
                            $this->addFlash('error', $message);
                        }
                    }
                } catch (ErrorException $e) {
                    echo $e->getMessage();
                }
            }
            elseif ($_GET['action'] === 'discord') {
                    $provider = new \Wohali\OAuth2\Client\Provider\Discord([
                        'clientId' => '940399103534333982',
                        'clientSecret' => '1tFCVjKVQCEj3LqVcURL9MLUO0Ux2EZu',
                        'redirectUri' => $this->urlPanel.'auth/connectAccount?action=discord'
                    ]);
                    if (!isset($_GET['code'])) {
                        // Step 1. Get authorization code
                        $authUrl = $provider->getAuthorizationUrl();
                        $_SESSION['oauth2state'] = $provider->getState();
                        return $this->redirect($authUrl);

                        // Check given state against previously stored one to mitigate CSRF attack
                    } elseif (empty($_GET['state']) || ($_GET['state'] !== $_SESSION['oauth2state'])) {
                        unset($_SESSION['oauth2state']);
                        exit('Invalid state');

                    } else {
                        // Step 2. Get an access token using the provided authorization code
                        $token = $provider->getAccessToken('authorization_code', [
                            'code' => $_GET['code']
                        ]);
                        $user = $provider->getResourceOwner($token);
                        // Vérifier si l'utilisateur à un compte discord
                        $userArray = $user->toArray();
                        if (!empty($userArray['id'])) {
                            $discordExist = $apiRequest->findBy(
                                'users',
                                'discordId',
                                $userArray['id']
                            )->getBody()->getContents();
                            if ($discordExist !== "[]") {
                                $this->addFlash('error', 'Ce discord est deja lié a un autre compte');
                                $this->redirectToRoute('auth_steam_discord');
                            } else {
                                $data = [
                                    "discordId" => $userArray['id']
                                ];
                                $response = $apiRequest->patch('users', $data, $sessionUser->getId());
                                if ($response->getStatusCode() === 200) {
                                    $this->getUser()->setDiscordId($userArray['id']);
                                    $discordHidden = true;
                                    $sessionUser->setDiscordId($userArray['id']);
                                    $this->addFlash(
                                        'success',
                                        'Votre Discord a été bien enregistrée'
                                    );
                                } else {
                                    $this->addFlash('error', 'Veuillez esseyer');
                                }
                            }
                        }
                    }
            }
        }
        if ($steamHidden && $discordHidden) {
            return $this->redirectToRoute('white_list');
        }
        return $this->render('auth_steam_discord/index.html.twig', [
            'controller_name' => 'AuthSteamDiscordController',
            'steamHidden' => $steamHidden,
            'discordHidden' => $discordHidden
        ]);
    }
}
