<?php

namespace App\Model;

use Doctrine\Common\Collections\ArrayCollection;

class WhiteList
{
    /** Questions **/
    private $whiteListResponse;

    public function __construct()
    {
        $this->whiteListResponse = new ArrayCollection();
    }

    /**
     * @return ArrayCollection
     */
    public function getWhiteListResponse(): ArrayCollection
    {
        return $this->whiteListResponse;
    }

    public function addWhiteListResponse(WhiteListResponse $whiteListResponse): self
    {
        if (!$this->whiteListResponse->contains($whiteListResponse)) {
            $this->whiteListResponse[] = $whiteListResponse;
        }

        return $this;
    }
}

