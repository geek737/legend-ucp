<?php

namespace App\Model;

class WhiteListQuestion
{
    /** ID **/
    private $id;

    /** Title **/
    private $title;

    /** Question **/
    private $question;

    /** Images **/
    private $images;

    /** Response **/
    private $response;

    /** Language **/
    private $language;

    /** Autheur **/
    private $author;

    public function __construct()
    {
        $this->images = [];
        $this->response = [];
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * @param mixed $question
     */
    public function setQuestion($question): void
    {
        $this->question = $question;
    }

    /**
     * @return array
     */
    public function getImages(): array
    {
        return $this->images;
    }

    /**
     * @param array $images
     */
    public function setImages(array $images): void
    {
        $this->images = $images;
    }

    /**
     * Add single image
     *
     * @param string $image
     */
    public function addImage(string $image): void
    {
        $this->images[] = $image;
    }

    public function removeImage(string $image): bool
    {
        $imageIndex = \array_search($image, $this->images);
        if ($imageIndex !== false) {
            array_splice($this->images, $imageIndex, 1);

            return true;
        }

        return false;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param mixed $language
     */
    public function setLanguage($language): void
    {
        $this->language = $language;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     */
    public function setAuthor($author): void
    {
        $this->author = $author;
    }

    /**
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param mixed $response
     */
    public function setResponse($response): void
    {
        $this->response = $response;
    }

    public function getWhiteListQuestionArray(): array
    {
        return get_object_vars($this);
    }

}

