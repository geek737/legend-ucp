<?php


namespace App\Model;


use Doctrine\Common\Collections\ArrayCollection;

class Language
{
    /** Id **/
    private $id;

    /** Label **/
    private $label;

    /** Code **/
    private $code;

    /** Questions **/
    private $whiteListSurveys;

    public function __construct()
    {
        $this->whiteListSurveys = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getWhiteListSurveys()
    {
        return $this->whiteListSurveys;
    }

    /**
     * @param mixed $whiteListSurveys
     */
    public function setWhiteListSurveys($whiteListSurveys): void
    {
        $this->whiteListSurveys = $whiteListSurveys;
    }

}