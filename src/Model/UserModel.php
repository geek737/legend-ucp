<?php

namespace App\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Users
 */
class UserModel
{
    /**
     * @var int
     * @Groups({"list_user_services", "show_user_services"})
     */
    private $id;

    /**
     * @var string|null
     * @Groups({"list_user_services", "show_user_services"})
     */
    private $identifier;

    /**
     * @var string
     * @Groups({"list_user_services", "show_user_services"})
     */
    private $firstName;

    /**
     * @var string|null
     * @Groups({"list_user_services", "show_user_services"})
     */
    private $lastName;

    /**
     * @var string|null
     * @Groups({"list_user_services", "show_user_services"})
     */
    private $birthDate;

    /**
     * @var string|null
     * @Groups({"list_user_services", "show_user_services"})
     */
    private $job;

    /**
     * @var int|null
     * @Groups({"list_user_services", "show_user_services"})
     */
    private $jobGrade;

    /**
     * @var UserService[]
     * @Groups({"list_user_services", "show_user_services"})
     */
    private $userServices;

    /**
     *  @var mixed
     * @Groups({"list_user_services", "show_user_services"})
     */
    private $totalServiceTime;

    /**
     *  @var mixed
     * @Groups({"list_user_services", "show_user_services"})
     */
    private $amountToPay;

    /**
     * @Groups({"list_user_services", "show_user_services"})
     */
    private $addonAccounts;

    public function __construct()
    {
        $this->userServices = new ArrayCollection();
        $this->addonAccounts = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    /**
     * @param string|null $identifier
     */
    public function setIdentifier(?string $identifier): void
    {
        $this->identifier = $identifier;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string|null $firstName
     */
    public function setFirstName(?string $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string|null $lastName
     */
    public function setLastName(?string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string|null
     */
    public function getBirthDate(): ?string
    {
        return $this->birthDate;
    }

    /**
     * @param string|null $birthDate
     */
    public function setBirthDate(?string $birthDate): void
    {
        $this->birthDate = $birthDate;
    }

    /**
     * @return string|null
     */
    public function getJob(): ?string
    {
        return $this->job;
    }

    /**
     * @param string|null $job
     */
    public function setJob(?string $job): void
    {
        $this->job = $job;
    }

    /**
     * @return int|null
     */
    public function getJobGrade(): ?int
    {
        return $this->jobGrade;
    }

    /**
     * @param int|null $jobGrade
     */
    public function setJobGrade(?int $jobGrade): void
    {
        $this->jobGrade = $jobGrade;
    }

    /**
     * @return UserService[]
     */
    public function getUserServices(): array
    {
        return $this->userServices;
    }

    /**
     * @param UserService[] $userServices
     */
    public function setUserServices(array $userServices): void
    {
        $this->userServices = $userServices;
    }

    /**
     * @return mixed
     */
    public function getTotalServiceTime()
    {
        return $this->totalServiceTime;
    }

    /**
     * @param mixed $totalServiceTime
     */
    public function setTotalServiceTime($totalServiceTime): void
    {
        $this->totalServiceTime = $totalServiceTime;
    }

    /**
     * @return mixed
     */
    public function getAmountToPay()
    {
        return $this->amountToPay;
    }

    /**
     * @param mixed $amountToPay
     */
    public function setAmountToPay($amountToPay): void
    {
        $this->amountToPay = $amountToPay;
    }

    /**
     * @return AddonAccount[]
     */
    public function getAddonAccounts(): array
    {
        return $this->addonAccounts;
    }

    /**
     * @param array $addonAccounts
     */
    public function setAddonAccounts(array $addonAccounts): void
    {
        $this->addonAccounts = $addonAccounts;
    }

}
