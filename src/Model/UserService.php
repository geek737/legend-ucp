<?php

namespace App\Model;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * User Services
 */
class UserService
{
    /**
     * @var int
     * @Groups({"list_user_services", "show_user_services"})
     */
    private $id;

    /**
     * @Groups({"list_user_services", "show_user_services"})
     */
    private $identifier;

    /**
     * @Groups({"list_user_services", "show_user_services"})
     */
    private $faction;

    /**
     * @Groups({"list_user_services", "show_user_services"})
     */
    private $service;

    /**
     * @Groups({"list_user_services", "show_user_services"})
     */
    private $timer;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    public function setIdentifier($identifier): self
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFaction()
    {
        return $this->faction;
    }

    public function setFaction($faction): self
    {
        $this->faction = $faction;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getService()
    {
        return $this->service;
    }

    public function setService($service): self
    {
        $this->service = $service;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTimer()
    {


        return $this->timer;
    }

    public function setTimer($timer): self
    {
        $this->timer = $timer;

        return $this;
    }

    /**
     * @ORM\PostLoad()
     */
    public function convertTimer()
    {
//        $this->setTimer(\date('m/d/Y H:i:s', $this->getTimer()));
    }
}
