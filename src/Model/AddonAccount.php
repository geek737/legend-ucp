<?php

namespace App\Model;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Addon Account
 */
class AddonAccount
{
    /**
     * @Groups({"list_user_services", "show_user_services"})
     */
    private $id;

    /**
     * @Groups({"list_user_services", "show_user_services"})
     */
    private $owner;

    /**
     * @Groups({"list_user_services", "show_user_services"})
     */
    private $faction;

    /**
     * @Groups({"list_user_services", "show_user_services"})
     */
    private $rib;

    /**
     * @Groups({"list_user_services", "show_user_services"})
     */
    private $current;

    /**
     * @Groups({"list_user_services", "show_user_services"})
     */
    private $money;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param mixed $owner
     */
    public function setOwner($owner): void
    {
        $this->owner = $owner;
    }

    /**
     * @return mixed
     */
    public function getFaction()
    {
        return $this->faction;
    }

    /**
     * @param mixed $faction
     */
    public function setFaction($faction): void
    {
        $this->faction = $faction;
    }

    /**
     * @return mixed
     */
    public function getRib()
    {
        return $this->rib;
    }

    /**
     * @param mixed $rib
     */
    public function setRib($rib): void
    {
        $this->rib = $rib;
    }

    /**
     * @return mixed
     */
    public function getCurrent()
    {
        return $this->current;
    }

    /**
     * @param mixed $current
     */
    public function setCurrent($current): void
    {
        $this->current = $current;
    }

    /**
     * @return mixed
     */
    public function getMoney()
    {
        return $this->money;
    }

    /**
     * @param mixed $money
     */
    public function setMoney($money): void
    {
        $this->money = $money;
    }
}
