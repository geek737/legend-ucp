<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\Ignore;

/**
 * User Services
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="user_service")
 * @ORM\Entity(repositoryClass="App\Repository\UserServicesRepository")
 */
class UserServices
{
    /**
     * @Groups({"list_user_services", "show_user_services"})
     * @ORM\GeneratedValue
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /**
     * @Groups({"list_user_services", "show_user_services"})
     * @ORM\Column(name="identifier", type="string", length=255)
     */
    private $identifier;

    /**
     * @Groups({"list_user_services", "show_user_services"})
     * @ORM\Column(name="faction", type="string", length=255)
     */
    private $faction;

    /**
     * @Groups({"list_user_services", "show_user_services"})
     * @ORM\Column(name="service", type="text")
     */
    private $service;

    /**
     * @Groups({"list_user_services", "show_user_services"})
     * @ORM\Column(name="timer", type="integer")
     */
    private $timer;

    /**
     * @ORM\ManyToOne(targetEntity="Users", inversedBy="userServices")
     * @ORM\JoinColumn(name="identifier", referencedColumnName="identifier")
     * @Groups({"list_user_services"})
     * @Ignore()
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    public function setIdentifier($identifier): self
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFaction()
    {
        return $this->faction;
    }

    public function setFaction($faction): self
    {
        $this->faction = $faction;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getService()
    {
        return $this->service;
    }

    public function setService($service): self
    {
        $this->service = $service;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTimer()
    {


        return $this->timer;
    }

    public function setTimer($timer): self
    {
        $this->timer = $timer;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    public function setUser($user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @ORM\PostLoad()
     */
    public function convertTimer()
    {
//        $this->setTimer(\date('m/d/Y H:i:s', $this->getTimer()));
    }
}
