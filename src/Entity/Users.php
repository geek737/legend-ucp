<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Users
 *
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="App\Repository\UsersRepository")
 */
class Users
{
//    /**
//     * @var int
//     * @Groups({"list_user_services", "show_user_services"})
//     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
//     * @ORM\GeneratedValue(strategy="IDENTITY")
//     */
//    private $id;

    /**
     * @var string|null
     * @Groups({"list_user_services", "show_user_services"})
     * @ORM\Id
     * @ORM\Column(name="identifier", type="string", length=255, nullable=true)
     */
    private $identifier;

    /**
     * @var string
     * @Groups({"list_user_services", "show_user_services"})
     * @ORM\Column(name="firstname", type="text")
     */
    private $firstName;

    /**
     * @var string|null
     * @Groups({"list_user_services", "show_user_services"})
     * @ORM\Column(name="lastname", type="string", length=255, nullable=true)
     */
    private $lastName;

    /**
     * @var string|null
     * @Groups({"list_user_services", "show_user_services"})
     * @ORM\Column(name="birth_date", type="string", length=255, nullable=true)
     */
    private $birthDate;

    /**
     * @var string|null
     * @Groups({"list_user_services", "show_user_services"})
     * @ORM\Column(name="job", type="string", length=255, nullable=true)
     */
    private $job;

    /**
     * @var int|null
     * @Groups({"list_user_services", "show_user_services"})
     * @ORM\Column(name="job_grade", type="integer", nullable=true)
     */
    private $jobGrade;

    /**
     * @Groups({"list_user_services", "show_user_services"})
     * @ORM\OneToMany(targetEntity="UserServices", mappedBy="user", fetch="EAGER")
     */
    private $userServices;

    /**
     * @Groups({"list_user_services", "show_user_services"})
     * @ORM\OneToMany(targetEntity="AddonAccount", mappedBy="user", fetch="EAGER")
     */
    private $addonAccounts;

    public function __construct()
    {
        $this->userServices = new ArrayCollection();
        $this->addonAccounts = new ArrayCollection();
    }

//    /**
//     * @return int
//     */
//    public function getId(): int
//    {
//        return $this->id;
//    }
//
//    /**
//     * @param int $id
//     */
//    public function setId(int $id): void
//    {
//        $this->id = $id;
//    }

    /**
     * @return string|null
     */
    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    /**
     * @param string|null $identifier
     */
    public function setIdentifier(?string $identifier): void
    {
        $this->identifier = $identifier;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string|null $firstName
     */
    public function setFirstName(?string $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string|null $lastName
     */
    public function setLastName(?string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string|null
     */
    public function getBirthDate(): ?string
    {
        return $this->birthDate;
    }

    /**
     * @param string|null $birthDate
     */
    public function setBirthDate(?string $birthDate): void
    {
        $this->birthDate = $birthDate;
    }

    /**
     * @return string|null
     */
    public function getJob(): ?string
    {
        return $this->job;
    }

    /**
     * @param string|null $job
     */
    public function setJob(?string $job): void
    {
        $this->job = $job;
    }

    /**
     * @return int|null
     */
    public function getJobGrade(): ?int
    {
        return $this->jobGrade;
    }

    /**
     * @param int|null $jobGrade
     */
    public function setJobGrade(?int $jobGrade): void
    {
        $this->jobGrade = $jobGrade;
    }

    /**
     * @return Collection|UserServices[]
     */
    public function getUserServices(): Collection
    {
        return $this->userServices;
    }

    public function addUserService(UserServices $userServices): self
    {
        if (!$this->userServices->contains($userServices)) {
            $this->userServices[] = $userServices;
            $userServices->setUser($this);
        }

        return $this;
    }

    public function removeUserService(UserServices $userServices): self
    {
        if ($this->userServices->removeElement($userServices)) {
            // set the owning side to null (unless already changed)
            if ($userServices->getUser() === $this) {
                $userServices->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|AddonAccount[]
     */
    public function getAddonAccounts(): Collection
    {
        return $this->addonAccounts;
    }

    public function addAddonAccount(AddonAccount $addonAccount): self
    {
        if (!$this->addonAccounts->contains($addonAccount)) {
            $this->addonAccounts[] = $addonAccount;
            $addonAccount->setOwner($this);
        }

        return $this;
    }

    public function removeAddonAccount(AddonAccount $addonAccount): self
    {
        if ($this->addonAccounts->removeElement($addonAccount)) {
            // set the owning side to null (unless already changed)
            if ($addonAccount->getOwner() === $this) {
                $addonAccount->getOwner(null);
            }
        }

        return $this;
    }
}
