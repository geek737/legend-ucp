<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Users
 *
 * @ORM\Table(name="vehicle_categories")
 * @ORM\Entity(repositoryClass="App\Repository\VehiclesCategoriesRepository")
 */
class VehiclesCategories
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(name="name", type="string", length=60, nullable=false)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(name="label", type="string", length=60, nullable=false)
     */
    private $label;

//    /**
//     * @ORM\OneToMany(targetEntity=Vehicles::class, mappedBy="category")
//     */
//    private $vehicles;

    public function __construct()
    {
//        $this->vehicles = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

//    /**
//     * @return Collection|Vehicles[]
//     */
//    public function getVehicles(): Collection
//    {
//        return $this->vehicles;
//    }
//
//    public function addVehicle(Vehicles $vehicle): self
//    {
//        if (!$this->vehicles->contains($vehicle)) {
//            $this->vehicles[] = $vehicle;
//            $vehicle->setCategory($this);
//        }
//
//        return $this;
//    }
//
//    public function removeVehicle(Vehicles $vehicle): self
//    {
//        if ($this->vehicles->removeElement($vehicle)) {
//            // set the owning side to null (unless already changed)
//            if ($vehicle->getCategory() === $this) {
//                $vehicle->setCategory(null);
//            }
//        }
//
//        return $this;
//    }
}
