<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\Ignore;

/**
 * User Services
 * @ORM\Table(name="addon_account")
 * @ORM\Entity(repositoryClass="App\Repository\AddonAccountRepository")
 */
class AddonAccount
{
    /**
     * @Groups({"list_user_services", "show_user_services"})
     * @ORM\GeneratedValue
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /**
     * @Groups({"list_user_services", "show_user_services"})
     * @ORM\Column(name="owner", type="string", length=255)
     */
    private $owner;

    /**
     * @Groups({"list_user_services", "show_user_services"})
     * @ORM\Column(name="faction", type="string", length=255)
     */
    private $faction;

    /**
     * @Groups({"list_user_services", "show_user_services"})
     * @ORM\Column(name="rib", type="string", length=50)
     */
    private $rib;

    /**
     * @Groups({"list_user_services", "show_user_services"})
     * @ORM\Column(name="courant", type="boolean")
     */
    private $current;

    /**
     * @Groups({"list_user_services", "show_user_services"})
     * @ORM\Column(name="money", type="integer")
     */
    private $money;

    /**
     * @ORM\ManyToOne(targetEntity="Users", inversedBy="AddonAccount")
     * @ORM\JoinColumn(name="owner", referencedColumnName="identifier")
     * @Groups({"list_user_services"})
     * @Ignore()
     */
    private $user;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param mixed $owner
     */
    public function setOwner($owner): void
    {
        $this->owner = $owner;
    }

    /**
     * @return mixed
     */
    public function getFaction()
    {
        return $this->faction;
    }

    /**
     * @param mixed $faction
     */
    public function setFaction($faction): void
    {
        $this->faction = $faction;
    }

    /**
     * @return mixed
     */
    public function getRib()
    {
        return $this->rib;
    }

    /**
     * @param mixed $rib
     */
    public function setRib($rib): void
    {
        $this->rib = $rib;
    }

    /**
     * @return mixed
     */
    public function getCurrent()
    {
        return $this->current;
    }

    /**
     * @param mixed $current
     */
    public function setCurrent($current): void
    {
        $this->current = $current;
    }

    /**
     * @return mixed
     */
    public function getMoney()
    {
        return $this->money;
    }

    /**
     * @param mixed $money
     */
    public function setMoney($money): void
    {
        $this->money = $money;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }
}
