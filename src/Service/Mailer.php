<?php


namespace App\Service;


use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Twig\Environment;

class Mailer
{
    /**
     * @var MailerInterface
     */
    private $mailer;
    /**
     * @var string
     */
    private $from;
    /**
     * @var Environment
     */
    private $twig;

    public function __construct(MailerInterface $mailer, string $from, Environment $twig)
    {
        $this->mailer = $mailer;
        $this->from = $from;
        $this->twig = $twig;
    }

    /**
     * Sends mails Rest Password Verification.
     */
    public function sendMailRestPasswordVerification(string $email, string $token)
    {
        try {
            $message = (new Email())
                ->from($this->from)
                ->to($email)
                ->subject('Reset password')
                ->html(
                    $this->twig->render(
                        'security/verification_message_send.html.twig',
                        ['token' => $token]
                    ),
                    'text/html'
                );

            $this->mailer->send($message);

        } catch (\Exception $e) {
            $e->getMessage();
        }
    }


    /**
     * Sends mails.
     *
     * @param string $email
     * @param string $subject
     * @param string $html
     * @throws TransportExceptionInterface
     */
    public function sendMail(string $email, string $subject, string $html)
    {
        try {
            $message = (new Email())
                ->from($this->from)
                ->to($email)
                ->subject($subject)
                ->html(
                    $this->twig->render(
                        $html
                    ),
                    'text/html'
                );

            $this->mailer->send($message);

        } catch (\Exception $e) {
            $e->getMessage();
        }
    }
}