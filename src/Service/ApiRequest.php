<?php


namespace App\Service;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

class ApiRequest
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var string
     */
    private $urlApi;

    /**
     * @var string
     */
    private $urlPanel;
    /**
     * @var FlashBagInterface
     */
    private $flash;
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var string
     */
    private $username;
    /**
     * @var string
     */
    private $password;

    public function __construct(
        string $urlApi,
        FlashBagInterface $flash,
        string $urlPanel,
        ContainerInterface $container,
        string $username,
        string $password
    ) {
        $this->client = new Client();
        $this->urlApi = $urlApi;
        $this->username = $username;
        $this->password = $password;
        $this->urlPanel = $urlPanel;
        $this->flash = $flash;
        $this->container = $container;
    }
    /**
     * Generates a URL from the given parameters.
     *
     * @see UrlGeneratorInterface
     */
    private function generateUrl(
        string $route,
        array $parameters = [],
        int $referenceType = UrlGeneratorInterface::ABSOLUTE_PATH
    ): string {
        return $this->container->get('router')->generate($route, $parameters, $referenceType);
    }

    /**
     * Returns a RedirectResponse to the given URL.
     */
    private function redirect(string $url, int $status = 302): RedirectResponse
    {
        return new RedirectResponse($url, $status);
    }

    /**
     * Returns a RedirectResponse to the given route with the given parameters.
     */
    private function redirectToRoute(string $route, array $parameters = [], int $status = 302): RedirectResponse
    {
        return $this->redirect($this->generateUrl($route, $parameters), $status);
    }

    /**
     * Get all from API
     *
     * @param string $table Table
     *
     * @throws GuzzleException
     */
    public function findAll(string $table)
    {
        $response = $this->client->request(
            'GET',
            "$this->urlApi$table",
            [
                "http_errors" => false,
                'json' => [
                    "grant_type" => "refresh_token",
                    "refresh_token" => $_COOKIE['refresh_token']
                ],
                "headers" => [
                    "accept" => "application/json",
                    "Authorization" => "Bearer ".$_COOKIE['token']
                ],
            ]
        );
        return $response;
    }

    /**
     * @param string $table       The table of this search
     * @param string $searchWith  The attribute that will be searched
     * @param string $searchValue Value to search
     * @param string $token       Token
     * @throws GuzzleException
     */
    public function findBy(string $table, string $searchWith, string $searchValue, string $token = null): ResponseInterface
    {
        $token = $token ?? $_COOKIE['token'];

        return $this->client->request(
            'GET',
            $this->urlApi."$table?$searchWith=$searchValue",
            [
                "http_errors" => false,
                'json' => [
                "grant_type" => "refresh_token",
                "refresh_token" => $_COOKIE['refresh_token']
            ],
                "headers" => [
                    "accept" => "application/json",
                    "Authorization" => "Bearer ".$token
                ],
            ]
        );
    }

    public function get(string $table, int $id)
    {
        $response = $this->client->request(
            'GET',
            $this->urlApi."$table/$id",
            [
                "http_errors" => false,
                "headers" => [
                    "accept" => "application/json",
                    "Authorization" => "Bearer ".$_COOKIE['token']
                ],
            ]
        );
        return $response;
    }

    /**
     * Method Post
     *
     * @param string $table       Table
     * @param array $data         Data to Send
     *
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public function post(string $table, array $data, $token = false): ResponseInterface
    {
        $options = [
            "http_errors" => false,
            "headers" => [
                "accept" => "application/json"
            ],
            'json' => $data
        ];
        if ($token) {
            $options["headers"]["Authorization"] = "Bearer ".$_COOKIE['token'];
        }
        return $this->client->request('POST',  $this->urlApi.''.$table, $options);

    }

    /**
     * Method Patch
     *
     * @param string $table Table
     * @param array  $data  Data to Send
     * @param int    $id    Id user
     * @param string $token Token
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public function patch(string $table, array $data, int $id, string $token = null): ResponseInterface
    {
        $token = $token ?? $_COOKIE['token'];
        return $this->client->request(
            'PATCH',
            $this->urlApi.''."$table/$id",
            [
                "http_errors" => false,
                "headers" => [
                    "accept" => "application/json",
                    "Content-Type" => "application/merge-patch+json",
                    "Authorization" => "Bearer ".$token
                ],
                'json' => $data
            ]
        );
    }

    /**
     * @param $response
     *
     * @return RedirectResponse
     */
    public function verifyStatusAndRedirect($response): RedirectResponse
    {
        if ($response->getStatusCode() === 401) {
            return $this->redirectToRoute('app_logout');
        }
    }

    public function generateToken()
    {
        $post = [
            "username" => $this->username,
            "password" => $this->password
        ];
        $response = $this->post('login', $post);
        $statusCode = $response->getStatusCode();
        if ($statusCode != 200) {
            throw new CustomUserMessageAuthenticationException('Login or password incorrect');
        }

        return \json_decode($response->getBody()->getContents(), true)['token'];
    }

}