<?php


namespace App\Service;


use App\Model\WhiteList;
use App\Model\WhiteListQuestion;
use App\Model\WhiteListResponse;

class WhiteListService
{
    /**
     * @var WhiteList
     */
    private $questionnaire;
    /**
     * @var \JsonMapper
     */
    private $mapper;

    public function __construct(WhiteList $questionnaire)
    {
        $this->questionnaire = $questionnaire;
        $this->mapper = new \JsonMapper();
    }

    public function addQuestionnaire($response, $user)
    {
        $questionsAPI = \json_decode($response->getBody()->getContents(), true);
//        foreach ($questionsAPI as $question) {
            $questionModel = new WhiteListQuestion();
            $this->mapper->map(
                \json_decode(\json_encode($questionsAPI)),
                $questionModel
            );
            $lineQuestion = new WhiteListResponse();
            $lineQuestion->setQuestion($questionModel);
            $lineQuestion->setUser($user);
            $lineQuestion->setCreatedAt(\date('Y-m-d H:i:s'));

            $this->questionnaire->addWhiteListResponse($lineQuestion);
//        }

        return $this->questionnaire;
    }
}