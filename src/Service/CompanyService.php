<?php


namespace App\Service;

use App\Model\UserModel;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

class CompanyService
{

    /**
     * @var SerializerInterface
     */
    private $serializer;
    const SALARY = [
            '0' => 90,
            '1' => 108,
            '2' => 120,
            '3' => 138,
            '4' => 150,
            '7' => 168,
            '8' => 198,
            '5' => 168,
            '6' => 180,
            '10' => 210,
            '11' => 228,
            '12' => 252,
            '13' => 270,
            '14' => 288,
            '16' => 330,
        ];

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * Serialize Object (To json).
     *
     * @param $data
     * @param $group
     *
     * @return string
     */
    public function serializeObject($data, $group): string
    {
        return $this->serializer->serialize(
            $data,
            JsonEncoder::FORMAT,
            [
                AbstractNormalizer::GROUPS => [$group],
                AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                    return null;
                },
                ObjectNormalizer::DISABLE_TYPE_ENFORCEMENT => true
            ]
        );
    }

    /**
     * Deserialize Object (To Object).
     *
     * @param mixed $data
     *
     * @return mixed
     */
    public function deserializeObject($data)
    {
        return $this->serializer->deserialize(
            $data, UserModel::class."[]", 'json', [
            ]
        );
    }

    /**
     * Filter data from date provided.
     *
     * @param $data
     * @param $group
     * @param $dateEnd
     * @param $dateBegin
     *
     * @return mixed
     */
    public function filterDataByDate($data, $group, $dateBegin, $dateEnd)
    {
        $result = $this->serializeObject($data, $group);
        $result = $this->deserializeObject($result);
        $salary = self::SALARY;
        \array_walk($result, function (UserModel &$user) use($dateEnd, $dateBegin, $salary){
            $userServices = [];
            $firstFound = null;
            $lastFound = null;
            $serviceOff = 0;
            $serviceOn = 0;
            $totalServiceTime = 0;
            $countUserServices = \count($user->getUserServices());
            foreach ($user->getUserServices() as $key => $userService) {
                if ($userService->getTimer() >= $dateBegin && $userService->getTimer() <= $dateEnd) {
                    $lastFound = $key;
                    if ($firstFound === null) {
                        $firstFound = $key;
                        if ($userService->getService() === 'OFF') {
                            $serviceOn = $user->getUserServices()[$firstFound - 1]->getTimer();
                            $userServices[] = $user->getUserServices()[$firstFound - 1];
                        }
                    }
                    $userService->getService() === 'OFF' ?
                        $serviceOff = $userService->getTimer() :
                        $serviceOn = $userService->getTimer();
                    $userServices[] = $userService;

                }
                if ($key === ($countUserServices - 1)) {
                    if (
                        $userServices[\count($userServices) - 1]->getService() === 'ON' &&
                        isset($user->getUserServices()[$lastFound + 1])
                    ) {
                        $serviceOff = $user->getUserServices()[$lastFound + 1]->getTimer();
                        $userServices[] = $user->getUserServices()[$lastFound + 1];
                    }
                }
                if ($serviceOff !== 0 && $serviceOn !== 0) {
                    $totalServiceTime += $serviceOff - $serviceOn;
                    $serviceOff = 0;
                    $serviceOn = 0;
                }
            }
            $user->setTotalServiceTime((int) round($totalServiceTime/60));
            $user->setAmountToPay((int) round(($salary[$user->getJobGrade()]/60) * $user->getTotalServiceTime()));
            $user->setUserServices(array_values($userServices));
        });

        return $result;
    }


}