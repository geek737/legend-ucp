<?php

namespace App\Security;

use App\Entity\FeedBack;
use App\Model\WhiteListResponse;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;


class User implements UserInterface, PasswordAuthenticatedUserInterface
{

    /** ID **/
    private $id;

    /**
     * @Assert\Email(
     *     message = "Votre email n'est pas valide."
     * )
     */
    private $email;

    /**
     * @Assert\Length(
     *     min=5,
     *     max = 20,
     *     minMessage="Cette valeur est trop courte. Elle devrait comporter 5 caractères ou plus.",
     *     maxMessage="Cette valeur est trop longue. Elle doit comporter 20 caractères ou moins."
     *     )
     * @Assert\Regex(pattern = "/^(?=[a-zA-Z0-9._\-ø]{5,20}$)(?!.*[_.]{2})[^_.].*[^_.]$/",
     *     message = "5 caractères minimum | 20 caractères maximum | Ne contient pas de caractères spéciaux")
     */
    private $userNameGame;

    /**
     * @Assert\Choice(
     *     {"accepted","pending","rejected","voice interview","none"},
     *     message="Cette valeur doit avoir 'accepted','pending','rejcted','voice interview','none'"
     *     )
     */
    private $statsWhitelist;

    /** Steam Id **/
    private $steamId;

    /** Discord Id **/
    private $discordId;

    /** Reponses **/
    private $whiteListResponses;

    /**
     * @Assert\Choice({"high","low"}, message="Cette valeur doit avoir 'high','low'")
     */
    private $priority;

    /**
     * @Assert\Type("\DateTimeInterface", message="Valeur n'est pas valide")
     */
    private $createdAt;

    /** Roles **/
    private $roles = [];

    /**
     * @Assert\Regex(
     *     pattern     = "/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d#?!@$%^&*-_]{6,}$/",
     *     message = "6 caractères minimum | 1 chiffre minimum | 1 lettre majuscule minimum."
     * )
     */
    private $password;

    /**
     * @Assert\EqualTo(propertyPath="password",message="Ces mots de passe ne correspondent pas. Veuillez réessayer.")
     */
    private $passwordChecked;
    /** if password will be changed **/
    private $isPasswordChanging;

    /** token to reset password **/
    private $tokenChangingPassword;

    /** date of generate url reset password **/
    private $urlCreatedAt;

    /** Feedback **/
    private $feedBacks;

    /** Feedback Created **/
    private $feedBacksWrited;

    /** Question created **/
    private $whiteListSurveys;


    public function __construct()
    {
        $this->whiteListResponses = new ArrayCollection();
        $this->feedBacks = new ArrayCollection();
        $this->feedBacksWrited = new ArrayCollection();
        $this->whiteListSurveys = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserNameGame()
    {
        return $this->userNameGame;
    }

    /**
     * @param mixed $userNameGame
     */
    public function setUserNameGame($userNameGame): void
    {
        $this->userNameGame = $userNameGame;
    }

    /**
     * @return mixed
     */
    public function getStatsWhitelist()
    {
        return $this->statsWhitelist;
    }

    /**
     * @param mixed $statsWhitelist
     */
    public function setStatsWhitelist($statsWhitelist): void
    {
        $this->statsWhitelist = $statsWhitelist;
    }

    /**
     * @return mixed
     */
    public function getSteamId()
    {
        return $this->steamId;
    }

    /**
     * @param mixed $steamId
     */
    public function setSteamId($steamId): void
    {
        $this->steamId = $steamId;
    }

    /**
     * @return mixed
     */
    public function getDiscordId()
    {
        return $this->discordId;
    }

    /**
     * @param mixed $discordId
     */
    public function setDiscordId($discordId): void
    {
        $this->discordId = $discordId;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $cratedAt
     */
    public function setCreatedAt($createdAt): void
    {
        $this->createdAt = $createdAt;
    }


    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->userNameGame;
    }

    /**
     * @deprecated since Symfony 5.3, use getUserIdentifier instead
     */
    public function getUsername(): string
    {
        return (string) $this->userNameGame;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function addRole(string $role): self
    {
        if (!\in_array($role, $this->roles)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPasswordChecked()
    {
        return $this->passwordChecked;
    }

    /**
     * @param mixed $passwordChecked
     */
    public function setPasswordChecked($passwordChecked): void
    {
        $this->passwordChecked = $passwordChecked;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return Collection|WhiteListResponse[]
     */
    public function getWhiteListResponses(): Collection
    {
        return $this->whiteListResponses;
    }

    public function addWhiteListResponse(WhiteListResponse $whiteListResponse): self
    {
        if (!$this->whiteListResponses->contains($whiteListResponse)) {
            $this->whiteListResponses[] = $whiteListResponse;
            $whiteListResponse->setUser($this);
        }

        return $this;
    }

    public function removeWhiteListResponse(WhiteListResponse $whiteListResponse): self
    {
        if ($this->whiteListResponses->removeElement($whiteListResponse)) {
            // set the owning side to null (unless already changed)
            if ($whiteListResponse->getUser() === $this) {
                $whiteListResponse->setUser(null);
            }
        }

        return $this;
    }

    public function getPriority(): ?string
    {
        return $this->priority;
    }

    public function setPriority(string $priority): self
    {
        $this->priority = $priority;

        return $this;
    }

    public function getUserArray()
    {
        return get_object_vars($this);
    }

    /**
     * @return Collection|WhiteListSurveys[]
     */
    public function getWhiteListSurveys(): Collection
    {
        return $this->whiteListSurveys;
    }

    public function addWhiteListSurvey(WhiteListSurveys $whiteListSurvey): self
    {
        if (!$this->whiteListSurveys->contains($whiteListSurvey)) {
            $this->whiteListSurveys[] = $whiteListSurvey;
            $whiteListSurvey->setAuthor($this);
        }

        return $this;
    }

    public function removeWhiteListSurvey(WhiteListSurveys $whiteListSurvey): self
    {
        if ($this->whiteListSurveys->removeElement($whiteListSurvey)) {
            // set the owning side to null (unless already changed)
            if ($whiteListSurvey->getAuthor() === $this) {
                $whiteListSurvey->setAuthor(null);
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsPasswordChanging()
    {
        return $this->isPasswordChanging;
    }

    /**
     * @param mixed $isPasswordChanging
     */
    public function setIsPasswordChanging($isPasswordChanging): void
    {
        $this->isPasswordChanging = $isPasswordChanging;
    }

    /**
     * @return mixed
     */
    public function getTokenChangingPassword()
    {
        return $this->tokenChangingPassword;
    }

    /**
     * @param mixed $tokenChangingPassword
     */
    public function setTokenChangingPassword($tokenChangingPassword): void
    {
        $this->tokenChangingPassword = $tokenChangingPassword;
    }

    /**
     * @return mixed
     */
    public function getUrlCreatedAt()
    {
        return $this->urlCreatedAt;
    }

    /**
     * @param mixed $urlCreatedAt
     */
    public function setUrlCreatedAt($urlCreatedAt): void
    {
        $this->urlCreatedAt = $urlCreatedAt;
    }
}
