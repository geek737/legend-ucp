<?php

namespace App\Security;

use App\Service\ApiRequest;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\SetCookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Authenticator\AbstractLoginFormAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\CsrfTokenBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\PasswordCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Authenticator\Passport\PassportInterface;
use Symfony\Component\Security\Http\Util\TargetPathTrait;
use Symfony\Contracts\HttpClient\HttpClientInterface;


class AppCustomAuthenticator extends AbstractLoginFormAuthenticator
{
    use TargetPathTrait;

    public const LOGIN_ROUTE = 'app_login';

    private $urlGenerator;
    /**
     * @var HttpClientInterface
     */
    private $client;
    /**
     * @var ApiRequest
     */
    private $apiRequest;

    public function __construct(UrlGeneratorInterface $urlGenerator, HttpClientInterface $client, ApiRequest $apiRequest)
    {
        $this->urlGenerator = $urlGenerator;
        $this->client = $client;
        $this->apiRequest = $apiRequest;
    }

    public function authenticate(Request $request): PassportInterface
    {
        /**
         *  Login
         */
        $username = $request->request->get('username');
        $post = [
                    "username" => $username,
                    "password" => $request->request->get('password')
        ];
        $response = $this->apiRequest->post('login', $post);
        $statusCode = $response->getStatusCode();
        if ($statusCode != 200) {
            throw new CustomUserMessageAuthenticationException('Login or password incorrect');
        }
        $response = \json_decode($response->getBody()->getContents(), true);
        $token = $response['token'];
        $refresh_token = $response['refresh_token'];
        $_COOKIE['token'] = $token;
        $_COOKIE['refresh_token'] = $refresh_token;
        $request->getSession()->set(Security::LAST_USERNAME, $username);
        return new Passport(
            new UserBadge($username),
            new PasswordCredentials($request->request->get('password')),
            [
                new CsrfTokenBadge('authenticate', $request->get('_csrf_token')),
            ]
        );
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        if ($targetPath = $this->getTargetPath($request->getSession(), $firewallName)) {
            return new RedirectResponse($targetPath);
        }
        return new RedirectResponse('/');
        // For example:
        //return new RedirectResponse($this->urlGenerator->generate('some_route'));
        throw new \Exception('TODO: provide a valid redirect inside '.__FILE__);
    }

    protected function getLoginUrl(Request $request): string
    {
        return $this->urlGenerator->generate(self::LOGIN_ROUTE);
    }
}
