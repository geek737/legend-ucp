<?php

namespace App\Repository;

use App\Entity\AddonAccount;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AddonAccount|null find($id, $lockMode = null, $lockVersion = null)
 * @method AddonAccount|null findOneBy(array $criteria, array $orderBy = null)
 * @method AddonAccount[]    findAll()
 * @method AddonAccount[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AddonAccountRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AddonAccount::class);
    }

    // /**
    //  * @return Users[] Returns an array of Users objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

//    public function findAllUserServices()
//    {
//        return $this->createQueryBuilder('us')
//            ->select(
//                'us.identifier, u.firstName, u.lastName, u.birthDate, us.faction, us.service, us.timer, u.jobGrade'
//            )
//            ->innerJoin(Users::class, 'u', 'WITH','u.identifier = us.identifier')
//            ->orderBy('us.identifier', 'ASC')
//            ->getQuery()
//            ->getArrayResult();
////            ->andWhere('u.exampleField = :val')
////            ->setParameter('val', $value)
////            ->getQuery()
////            ->getOneOrNullResult()
//        ;
//    }
//
//    public function findUserServices(string $identifier)
//    {
//        return $this->createQueryBuilder('us')
//            ->select(
//                'us.identifier, u.firstName, u.lastName, u.birthDate, us.faction, us.service, us.timer, u.jobGrade'
//            )
//            ->innerJoin(Users::class, 'u', 'WITH','u.identifier = us.identifier')
//            ->andWhere('us.identifier = :val')
//            ->setParameter('val', $identifier)
//            ->orderBy('us.identifier', 'ASC')
//            ->getQuery()
//            ->getArrayResult();
////            ->getQuery()
////            ->getOneOrNullResult()
//        ;
//    }
}
