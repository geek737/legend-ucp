<?php

namespace App\Repository;

use App\Entity\Users;
use App\Entity\UserServices;
use App\Entity\AddonAccount;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Users|null find($id, $lockMode = null, $lockVersion = null)
 * @method Users|null findOneBy(array $criteria, array $orderBy = null)
 * @method Users[]    findAll()
 * @method Users[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UsersRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Users::class);
    }

    // /**
    //  * @return Users[] Returns an array of Users objects
    //  */
    public function getPoliceUsers(int $dateBegin, int $dateEnd, string $job)
    {
        $queryBuilder = $this->createQueryBuilder('u');
        $expr = $queryBuilder->expr();
        $betweenDate = $expr->between('us.timer', ':begin', ':end');
        $result = $queryBuilder
            ->innerJoin(UserServices::class, 'us', 'WITH','u.identifier = us.user')
            ->innerJoin(AddonAccount::class, 'ad', 'WITH','u.identifier = ad.user')
            ->where('u.job = :job')
            ->andWhere($betweenDate)
            ->setParameter('job', $job)
            ->setParameter('begin', $dateBegin)
            ->setParameter('end', $dateEnd)
            ->orderBy('u.jobGrade', 'DESC')
            ->getQuery()
            ->useQueryCache(true);

        return $result->getResult()
        ;
    }

    /*
    public function findOneBySomeField($value): ?Users
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
